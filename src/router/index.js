var express = require("express");
var globals = require('../variables');
var router = express.Router();
const swaggerUi = require('swagger-ui-express');
const InfoController= require('../controller/InfoController');
const SlotController= require('../controller/SlotController');
const swaggerDocument = require('../variables/swagger.json');
//Router Main(Home)
router.use('/', swaggerUi.serve);
router.get('/', swaggerUi.setup(swaggerDocument));
/*
The Endpoint for Park a Car
body: {"carNumber":"50C"}
*/
router.post("/park", function (req, res) {
  SlotController.park(req, res,  globals);
});
/*
The Endpoint for Unpark the Car
body: {"carNumber":"50D"}
*/
router.post("/unpark", function (req, res) {
  SlotController.unpark(req, res,  globals);
});
/*
The Endpoint get the Car/Slot Information
query string: carNumber=50C || slotNumber=1
*/
router.get("/info", function (req, res) {
  InfoController.main(req, res,  globals);
});

module.exports = router;

const fs = require("fs");
const path = require("path");
var globals = require("../variables");
const ItemSlot = require("./ItemSlot");
class Model {
  static FILE='./data/car-packing.json';

  static ensureDirectoryExistence(filePath, data) {
    var dirname = path.dirname(filePath);
    if (!fs.existsSync(dirname)){
      fs.mkdirSync(dirname);
      fs.writeFileSync(filePath, data);
    }
  }

  static createSlot() {
    Model.ensureDirectoryExistence(Model.FILE,JSON.stringify([]));
    let olddata = [];
    let rawdata = fs.readFileSync(Model.FILE);
    
    if (rawdata) olddata = JSON.parse(rawdata);
    const length = olddata.length;
    if (length != process.env.SIZE) {
      
      if (length < process.env.SIZE) {
        for (let index = length;  index < process.env.SIZE;index++) {
          olddata.push(ItemSlot(index + 1, null, null));
        }
      } else {
        for (let index = process.env.SIZE;  index < length;index++) {
          olddata.splice(process.env.SIZE, 1);
        }
      }
    }
    globals.SLOT = olddata;
    Model.updateFile();
  }

  static updateFile() {
    let data = JSON.stringify(globals.SLOT);
    fs.writeFileSync(Model.FILE, data);
  }
}
module.exports = Model;

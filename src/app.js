require("dotenv").config();
var Express = require("express");
var Model = require("./model");
var router = require("./router");
var bodyParser = require("body-parser");
var app = new Express();
var http = require("http").createServer(app);
const rateLimit = require("express-rate-limit");
const LimitController = require("./controller/LimitController");
const limiter = rateLimit({
  windowMs: process.env.LIMIT_TIME * 1000,
  max: process.env.LIMIT_REQUEST,
  handler: function (req, res) {
    LimitController.main(req, res);
  }
});
app.use(limiter);
app.use(bodyParser.json());
app.use("/", router);
//Listen app on port in ENV
http.listen(process.env.PORT, function () {
  Model.createSlot(); //Create list of slot for Car
  console.log("Smart parking listening on port " + process.env.PORT + "!");
});

var Globals = {
    SLOT:[],
    SUCCESS:'success',
    ERROR:'error',
    ERROR_MESSAGE:{
        require_car_name:'Please input car number',
        require_car_or_slot:'Please input car number or slot number',
        car_park_full:'The car park is full',
        car_not_found:'The car is not found in parked',
        car_is_exits:'The car number is exist in parked',
        limit_connection:'Too many connection from this IP, please try again later'
    }
}
module.exports = Globals;
class InfoController {
  static main(req, res, globals) {
    try {
      const carNumber = req.query.carNumber;
      const slotNumber = req.query.slotNumber;
      if(!carNumber && !slotNumber) throw globals.ERROR_MESSAGE.require_car_or_slot;
      let  itemSlot=null;
      globals.SLOT.forEach(slot => {
        if((carNumber && slot.carNumber==carNumber )
          || (slotNumber && slot.slotNumber==slotNumber))
        {
          itemSlot  = slot;
        }
      });
      if(itemSlot===null) throw globals.ERROR_MESSAGE.car_not_found;
      return res.json({ status: globals.SUCCESS, slot: itemSlot });
    } catch (ex) {
      return res.json({ status: globals.ERROR, message: ex });
    }
  }
}
module.exports = InfoController;

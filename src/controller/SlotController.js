const { updateFile } = require('../model');
const ItemSlot= require('../model/ItemSlot');
class SlotController {

  static park(req, res, globals) {
    try {
      const carNumber = req.body.carNumber;
      if(!carNumber) throw globals.ERROR_MESSAGE.require_car_name;
      let slotNumber=-1;
      globals.SLOT.forEach((slot,index) => {
        if(slot.carNumber===null&& slotNumber===-1)
        {
          slotNumber = slot.slotNumber;
          globals.SLOT[index]= ItemSlot(slotNumber,carNumber,new Date());
          updateFile();
        }else if(slot.carNumber===carNumber){
          throw globals.ERROR_MESSAGE.car_is_exits;
        }
      });
      if(slotNumber===-1) throw globals.ERROR_MESSAGE.car_park_full;
      return res.json({ status: globals.SUCCESS ,slotNumber});
    } catch (ex) {
      return res.json({ status: globals.ERROR, message: ex });
    }
  }


  static unpark(req, res, globals) {
    try {
      const carNumber = req.body.carNumber;
      if(!carNumber) throw globals.ERROR_MESSAGE.require_car_name;
      let slotNumber=-1;
      globals.SLOT.forEach((slot,index) => {
        if(slot.carNumber===carNumber&& slotNumber===-1)
        {
          slotNumber = slot.slotNumber;
          globals.SLOT[index]= ItemSlot(slotNumber,null,null);
          updateFile();
          return;
        }
      });
      if(slotNumber===-1) throw globals.ERROR_MESSAGE.car_not_found;
      return res.json({ status: globals.SUCCESS ,slotNumber});
    } catch (ex) {
      return res.json({ status: globals.ERROR, message: ex });
    }
  }
}
module.exports = SlotController;

var globals = require("../variables");
class LimitController {
  static main(req, res) {
    return res.json({ status: globals.ERROR, message: globals.ERROR_MESSAGE.limit_connection ,ip: req.ip});
  }
}
module.exports = LimitController;

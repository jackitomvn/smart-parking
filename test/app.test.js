require("dotenv").config();
var Chai = require("chai");
var ChaiClient = require("chai-http");
var Expect = Chai.expect;
Chai.use(ChaiClient);
describe("Car Parking Backend Testing", () => {
  describe("API Park", () => {
    it("With none data", (done) => {
      Chai.request(process.env.SERVER_TEST)
        .post("/park")
        .send({})
        .end((err, res) => {
          Expect(err).to.be.null;
          Expect(res).to.have.status(200);
          done();
        });
    });

    it("With real data", (done) => {
      Chai.request(process.env.SERVER_TEST)
        .post("/park")
        .send({ carNumber: "30A-14970" })
        .end((err, res) => {
          Expect(err).to.be.null;
          Expect(res).to.have.status(200);
          done();
        });
    });

    it("With extis data", (done) => {
      Chai.request(process.env.SERVER_TEST)
        .post("/park")
        .send({ carNumber: "30A-14970" })
        .end((err, res) => {
          Expect(err).to.be.null;
          Expect(res).to.have.status(200);
          done();
        });
    });
  });

  describe("API UnPark", () => {
    it("With none data", (done) => {
      Chai.request(process.env.SERVER_TEST)
        .post("/unpark")
        .send({})
        .end((err, res) => {
          Expect(err).to.be.null;
          Expect(res).to.have.status(200);
          done();
        });
    });

    it("With real data", (done) => {
      Chai.request(process.env.SERVER_TEST)
        .post("/unpark")
        .send({ carNumber: "30A-14970" })
        .end((err, res) => {
          Expect(err).to.be.null;
          Expect(res).to.have.status(200);
          done();
        });
    });

    it("With extis data", (done) => {
      Chai.request(process.env.SERVER_TEST)
        .post("/unpark")
        .send({ carNumber: "30A-14970" })
        .end((err, res) => {
          Expect(err).to.be.null;
          Expect(res).to.have.status(200);
          done();
        });
    });
  });

  describe("API Info", () => {
    it("With no carNumber and no slotNumber", (done) => {
      Chai.request(process.env.SERVER_TEST)
        .get("/info")
        .end((err, res) => {
          Expect(err).to.be.null;
          Expect(res).to.have.status(200);
          done();
        });
    });

    it("With carNumber", (done) => {
      Chai.request(process.env.SERVER_TEST)
        .get("/info?carNumber=30A-14970")
        .end((err, res) => {
          Expect(err).to.be.null;
          Expect(res).to.have.status(200);
          done();
        });
    });

    it("With slotNumber", (done) => {
      Chai.request(process.env.SERVER_TEST)
        .get("/info?slotNumber=1")
        .end((err, res) => {
          Expect(err).to.be.null;
          Expect(res).to.have.status(200);
          done();
        });
    });
  });


});

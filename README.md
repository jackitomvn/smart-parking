### Description ###

* This is Smart Parking Backend.
* Version: 1.0.0
* Nguyen Giang Nam. Email: lymo0501@gmail.com. Mobile:+84.973.589.615 

### How do I get set up? ###

* Get project from git
* `git clone https://jackitom@bitbucket.org/jackitomvn/smart-parking.git`
* Install all package
* `npm install`
* Run project
* `npm start`


### How do I run on docker? ###

* Run with docker compose
* `docker-compose up`
* Run with SH file
* `sh run.sh`
